### Célok
 1. Diagnózis felállítása, ismertetése
 2. Szakmai és marketingcsapat létrehozása
 3. Szakmai vita generálása
 4. Társadalmi tartozás ismertetése / elismertetése
 5. Csapattal kivitelezés módjának meghatározása
 6. Megfelelő projektek publikálása közösségi finanszírozási portálokon

### Belépő feladat minden önkéntes számára

Keressen hasonló élethelyzetű embert.
Keressen olyan baráti kört, akit inspirál a feladat.
